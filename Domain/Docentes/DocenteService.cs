﻿using Entities.Docentes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Professores
{
    public class DocenteService
    {
        public IDocente Edita(string docente)
        {
            return new DocenteModel();
        }

        public void Atualiza(IDocente docente)
        {
            if (Existe(docente.Docente))
            {
                //Atualiza
            }
            else
            {
                //Novo Registo
            }

        }

        public bool Existe(string docente)
        {
            return true;
        }

        public dynamic DaValorAtributos(string docente, string[] atributos)
        {
            if (!Existe(docente))
            {
                return null;
            }
            else
            {
                return new object();
            }
        }

        public List<IDocente> ListaProfessores()
        {
            return new List<IDocente>();
        }

        public void Anula(string docente)
        {
            if (Existe(docente))
            {
                //Anula
            }
            else
            {
                //Exception
            }
        }
    }
}
