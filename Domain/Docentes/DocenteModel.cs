﻿using Entities.Docentes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Domain.Professores
{
    public class DocenteModel: IDocente
    {

        public string Docente { get; set; }
        public string Nome { get; set; }
        public string Identificacao { get; set; }
        public string NumeroIdentificacao { get; set; }
        public string EstadoCivil { get; set; }
        public string Escolaridade { get; set; }
        
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string Endereco { get; set; }
    }
}
