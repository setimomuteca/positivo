﻿using Entities.Estudantes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Estudantes
{
    public interface IEstudanteUseCase
    {
        IEstudante Edita(string estudante);
        void Atualiza(IEstudante estudante);

        bool Existe(string estudante);
        void Delete(string estudante);
        List<IEstudante> ListaEstudantes();


    }
}
