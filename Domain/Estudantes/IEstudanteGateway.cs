﻿using Entities.Estudantes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Estudantes
{
    public interface IEstudanteGateway
    {
        
        
        void Atualiza(IEstudante estudante);
        List<IEstudante> GetEstudantes();
        IEstudante Edita(string estudante);
        void Remove(string estudante);
        void SetEstudante(IEstudante estudante);

    }
}
