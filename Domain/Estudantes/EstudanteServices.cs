﻿using System;
using System.Collections.Generic;
using DevExpress.Xpo;
using Entities.Estudantes;

namespace Domain.Estudantes
{

    public class EstudanteServices: IEstudanteUseCase
    {
        IEstudanteGateway repository;
        public EstudanteServices(IEstudanteGateway repository)
        {
            this.repository = repository;
        }
        public IEstudante Edita(string estudante)
        {
            return null;
        }

        public void Atualiza(IEstudante estudante)
        {
            if (Existe(estudante.Estudante))
            {
               //Atualiza
            }
            else
            {
                repository.Atualiza(estudante);
            }

        }

        public bool Existe(string estudante)
        {
            return true;
        }

        public dynamic  DaValorAtributos(string estudante, string[] atributos)
        {
            if (!Existe(estudante))
            {
                return null;
            }
            else
            {
                return new object();
            }
        }

        public List<IEstudante> ListaEstudantes()
        {
            return repository.GetEstudantes();
        }

        public void Anula(string estudante)
        {
            if (Existe(estudante))
            {
                //Anula
            }
            else
            {
                //Exception
            }
        }

        public void Delete(string estudante)
        {
            throw new NotImplementedException();
        }
    }

}