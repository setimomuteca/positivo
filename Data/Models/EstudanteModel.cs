﻿using Entities.Estudantes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{    
    public class EstudanteModel:IEstudante
    {                        
        public string Estudante { get; set; }
        
        public string Nome { get; set; }
        public DateTime DataNascimento { get; set; }                
        public bool Responsavel { get; set; }
        public string Foto { get; set; }
        public char Sexo { get; set; }
        public string Identificacao { get; set; }
        public string NumIdentificacao { get; set; }
        public string Nacionalidade { get; set; }
        public string Naturalidade { get; set; }
        public string Email { get; set; }
        public string Telefone1 { get; set; }
        public string Telefone2 { get; set; }
        public string Endereco { get; set; }
        public string Pai { get; set; }
        public string Mae { get; set; }
    }
}
