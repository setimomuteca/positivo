﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    [Table("Encarregados")]
    public class EncarregadoModel
    {
        [Key]
       
       public  string Encarregado { get; set; }
       [Required]
        public string Nome { get; set; }

    }
}
