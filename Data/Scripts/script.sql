

IF NOT EXISTS (SELECT 1 FROM sys.databases WHERE name='POSITIVO')
BEGIN 	
	CREATE DATABASE POSITIVO
END 
GO
USE POSITIVO

--====================================================================================
--TABELAS
--====================================================================================
--ESTUDANTES
IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE Name='Estudantes' AND TYPE='U')
BEGIN
	CREATE TABLE Estudantes
	(
		Estudante NVARCHAR(50) NOT NULL, 
		Nome NVARCHAR (100) NOT NULL,
		DataNascimento DATETIME NOT NULL,
		Identificacao  NVARCHAR (15) NOT NULL,
		NumIdentificacao NVARCHAR(30),
		Nacionalidade NVARCHAR(30),
		Email NVARCHAR(50),
		Telefone1 NVARCHAR(30),
		Telefone2 NVARCHAR(30),
		Endereco NVARCHAR(50) ,
		Sexo CHAR,
		Pai NVARCHAR(100),
		Mae NVARCHAR(100) 
	 )
END 


