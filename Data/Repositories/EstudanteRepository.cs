﻿using Data.Models;
using Domain.Estudantes;
using Entities.Estudantes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class EstudanteRepository : IEstudanteGateway
    {
        private readonly Utilitarios utilitarios;

        SqlCommand SqlCmd = new SqlCommand();

        public EstudanteRepository()
        {
            utilitarios = new Utilitarios();
        }
        public void Atualiza(IEstudante e)
        {            
            bool trancsaccao = false;
            SqlTransaction sqlTransaction = null;

            try
            {
                string query = string.Format("INSERT INTO Estudantes VALUES('{0}', '{1}', {2}, '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}','{12}')",
                                                                       e.Estudante, e.Nome, e.DataNascimento, e.Identificacao, e.NumIdentificacao, e.Nacionalidade,
                                                                       e.Email, e.Telefone1, e.Telefone2, e.Endereco, e.Sexo, e.Pai, e.Mae);

                SqlCmd.Connection = utilitarios.SqlConnection;
                SqlCmd.CommandType = CommandType.Text;

                if (utilitarios.SqlConnection.State == ConnectionState.Closed) { utilitarios.SqlConnection.Open(); }

                SqlCmd.CommandText = query;



                sqlTransaction = utilitarios.SqlConnection.BeginTransaction();
                SqlCmd.Transaction = sqlTransaction;

                SqlCmd.ExecuteNonQuery();
            }
            finally
            {
                if (trancsaccao == true) { sqlTransaction.Rollback(); }
                trancsaccao = false;

                if (utilitarios.SqlConnection.State == ConnectionState.Open) { utilitarios.SqlConnection.Close(); }
                SqlCmd.Parameters.Clear();
            }           
        }

        public IEstudante Edita(string estudante)
        {
            throw new NotImplementedException();
        }

        public List<IEstudante> GetEstudantes()
        {
            bool trancsaccao = false;
            SqlTransaction sqlTransaction = null;
            List<IEstudante> estudantes = new List<IEstudante>();

            try
            {
                estudantes.Add(
                    new EstudanteModel()
                    {
                        Estudante = "000",
                        Nome = "Eurico Lena Adão",
                        Pai = "Jos Maria da SIlva"
                    }

                    );
                estudantes.Add(
                   new EstudanteModel()
                   {
                       Estudante = "000",
                       Nome = "Eurico Lena Adão",
                       Pai = "Jos Maria da SIlva"
                   }

                   );
                estudantes.Add(
                   new EstudanteModel()
                   {
                       Estudante = "000",
                       Nome = "Eurico Lena Adão",
                       Pai = "Jos Maria da SIlva"
                   }

                   );
                estudantes.Add(
                   new EstudanteModel()
                   {
                       Estudante = "000",
                       Nome = "Eurico Lena Adão",
                       Pai = "Jos Maria da SIlva"
                   }

                   );
                estudantes.Add(
                   new EstudanteModel()
                   {
                       Estudante = "000",
                       Nome = "Eurico Lena Adão",
                       Pai = "Jos Maria da SIlva"
                   }

                   );
                estudantes.Add(
                   new EstudanteModel()
                   {
                       Estudante = "000",
                       Nome = "Eurico Lena Adão",
                       Pai = "Jos Maria da SIlva"
                   }

                   );
                //string query = "SELECT* FROM Estudantes";

                //SqlCmd.Connection = utilitarios.SqlConnection;
                //SqlCmd.CommandType = CommandType.Text;

                //if (utilitarios.SqlConnection.State == ConnectionState.Closed) { utilitarios.SqlConnection.Open(); }

                //SqlCmd.CommandText = query;



                //sqlTransaction = utilitarios.SqlConnection.BeginTransaction();
                //SqlCmd.Transaction = sqlTransaction;

                //SqlCmd.ExecuteNonQuery();
            }
            finally
            {
                //if (trancsaccao == true) { sqlTransaction.Rollback(); }
                //trancsaccao = false;

                //if (utilitarios.SqlConnection.State == ConnectionState.Open) { utilitarios.SqlConnection.Close(); }
                //SqlCmd.Parameters.Clear();
            }
            return estudantes;
        }

        public void Remove(string estudante)
        {
            throw new NotImplementedException();
        }

        public void SetEstudante(IEstudante estudante)
        {
            throw new NotImplementedException();
        }       
    }
}
