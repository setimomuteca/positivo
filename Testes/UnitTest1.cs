﻿using System;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testes
{
    [TestClass]
    public class UnitTest1
    {
       

        [TestMethod]
        public void TestMethod1()
        {
            Calculos calc = new Calculos();
            int num1 = 5;
            int num2 = 10;

            int resultado = calc.DevolveSoma(num1, num2);

            Assert.AreEqual(15, resultado);
        }
    }
}
