﻿using Entities.Estudantes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserInterface.UIEstudantes
{
    public interface IEstudanteList
    {
        
          List<IEstudante> GirdAlunos { get; set; }
        void Show();
    }
}
