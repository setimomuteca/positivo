﻿using Domain.Estudantes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.UIEstudantes
{
    public class EstudantePresenter
    {
        private IEstudanteView view;
        private IEstudanteList listaEstudantes;
        private IEstudanteUseCase usecase;

        public EstudantePresenter(IEstudanteView view, IEstudanteList listaEstudantes, IEstudanteUseCase usecase)
        {
            this.view = view;
            this.listaEstudantes = listaEstudantes;
            this.usecase = usecase;


            listaEstudantes.GirdAlunos = usecase.ListaEstudantes();
            listaEstudantes.Show();
        }
    }
}
