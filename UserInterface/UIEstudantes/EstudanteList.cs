﻿using Domain.Estudantes;
using Entities.Estudantes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserInterface.UIEstudantes
{
    public partial class EstudanteList : Form, IEstudanteList
    {
       // EstudanteServices estudante = new EstudanteServices();//TODO.AM - Dependecy Injection
        public EstudanteList()
        {
            InitializeComponent();
        }

        public List<IEstudante> GirdAlunos { get => (List<IEstudante>)gridEstudantes.DataSource; set => gridEstudantes.DataSource = value; }

        private void AlunoList_Load(object sender, EventArgs e)
        {
          //  gridEstudantes.DataSource = estudante.ListaEstudantes();
        }
    }
}
