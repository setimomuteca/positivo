﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserInterface.UIEstudantes
{
    public partial class EstudanteView : Form, IEstudanteView
    {
        public EstudanteView()
        {
            InitializeComponent();
        }

        private void btnLista_Click(object sender, EventArgs e)
        {
            EstudanteList alunoList = new EstudanteList();

            alunoList.Show();
        }

        private void AlunosView_Load(object sender, EventArgs e)
        {

        }
    }
}
