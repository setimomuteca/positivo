﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Disciplinas
{
    public interface IDisciplina
    {
        string  Disciplina { get; set; }
        string Descricao { get; set; }

        bool Anulado { get; set; }


    }
}
