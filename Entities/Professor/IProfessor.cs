﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Professor
{
    public interface IProfessor
    {
        string Professor { get; set; }
        string Nome { get; set; }
        DateTime DataNascimento { get; set; }
        string Nacionalidade { get; set; }
        string Identificacao { get; set; }
        
    }
}
