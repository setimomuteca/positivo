﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Docentes
{
    public interface IParametros
    {
        bool ObrigaIdentificacao { get; set; }
        bool PermiteNumeroRepetido { get; set; }

    }
}
