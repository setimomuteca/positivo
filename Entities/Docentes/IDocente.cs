﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Docentes
{
    public interface IDocente
    {
        string Docente { get; set; }
        string Nome { get; set; }
        string Identificacao { get; set; }
        string NumeroIdentificacao { get; set; }
        string EstadoCivil { get; set; }
        string Escolaridade { get; set; }

        string Telefone { get; set; }
        string Email { get; set; }
        string Endereco { get; set; }

    }
}
