﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Estudantes
{
    
    public interface IEstudante
    {
        string Estudante { get; set; }
        string Nome { get; set; }
        DateTime DataNascimento { get; set; }      
        bool Responsavel { get; set; }
        string Foto { get; set; }
        char Sexo { get; set; }
        string Identificacao { get; set; }
        string NumIdentificacao { get; set; }
        string Nacionalidade { get; set; }
        string Naturalidade { get; set; }
        string Email { get; set; }
        string Telefone1 { get; set; }
        string Telefone2 { get; set; }
        string Endereco { get; set; }        
        string Pai { get; set; }
        string Mae { get; set; }

    }
}
